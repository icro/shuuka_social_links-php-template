<?php


function CallAPI($method, $url, $data = false)
{
    try{
        $curl = curl_init();
        $token = (isset($_COOKIE["token"])) ? 'Bearer '.$_COOKIE["token"] : 'Bearer No Access Denied';

        switch ($method)
        {
            case "POST":
                if ($data){
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); //immer da gewessen
                    curl_setopt($curl, CURLOPT_POSTREDIR, 3);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            default:
                if ($data){
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        // Optional Authentication:
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: '.$token));


        $result = curl_exec($curl);

        curl_close($curl);
    }catch(Exception $e){
        return $e;
    }


    return $result;

}

?>