# Shuuka social links template

#### Description
Shuuka social link help you to display all your social link in one single page. Your own bio link for instagram and other social media. 
You need only a shuuka account.

**If you do not like the template or want another design you can change it freely to your liking**

#### Installation 
1. Download [the latest release](https://gitlab.com/icro/shuuka_social_links-php-template) and copy the files to your folder.
	> Enter with FTP to your root of your page. 
	> Create a new folder and give it the name that you want for the  slug
2. Open the file secreteKey.php and put your key to the variable and save it.
	```
		<?php
			$shuukaSecreteKey = '[YOUR KEY]';
		?>
	```
3. Open your browser and call your domain example.com/slug

Now you have to see your Shuuka list.

-------
If you don't have a key log in to shuuka.com and go to https://www.shuuka.com/de/user/update/settings and you would see the key there